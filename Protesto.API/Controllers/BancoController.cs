﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Protesto.BLL.Models;
using Protesto.DAL;

namespace Protesto.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BancoController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BancoController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/banco
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Banco>>> GetBancos()
        {
            return await _context.Banco.ToListAsync();
        }

        // GET: api/banco/5 
        [HttpGet("{id}")]
        public async Task<ActionResult<Banco>> GetBanco(int id)
        {
            var user = await _context.Banco.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return user;
        }

        // POST api/banco
        [HttpPost]
        public async Task<ActionResult<Banco>> PostBanco(Banco banco)
        {
            // Verifica se o codigo do banco já existe
            Banco bancoCad = BancoExisteCodigo(banco.CodigoBanco);
            if (bancoCad != null)
            {
                if (bancoCad.BancoId > 0)
                {
                    return CreatedAtAction("GetBanco", new { id = bancoCad.BancoId }, bancoCad);
                }
            }

            // Verifica se o nome do banco já existe
            bancoCad = BancoExisteNome(banco.Nome);
            if (bancoCad != null)
            {
                if (bancoCad.BancoId > 0)
                {
                    return CreatedAtAction("GetBanco", new { id = bancoCad.BancoId }, bancoCad);
                }
            }

            _context.Banco.Add(banco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBanco", new { id = banco.BancoId }, banco);
        }

        // PUT api/banco/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBanco(int id, Banco banco)
        {
            if (id != banco.BancoId)
            {
                return BadRequest();
            }

            if (BancoExiste(id))
            {
                _context.Entry(banco).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetBanco", new { id = banco.BancoId }, banco);
                }
                catch (DbUpdateConcurrencyException)
                {
                    return BadRequest();
                }
            }
            else
            {
                return NotFound();
            }
        }

        // DELETE api/banco/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Banco>> DeleteBanco(int id)
        {
            var banco = await _context.Banco.FindAsync(id);
            if (banco == null)
            {
                return NotFound();
            }
            _context.Banco.Remove(banco);
            await _context.SaveChangesAsync();
            return banco;
        }

        private bool BancoExiste(int id)
        {
            return _context.Banco.Any(e => e.BancoId == id);
        }

        private Banco BancoExisteCodigo(int codigo)
        {
            return _context.Banco.FirstOrDefault(b => b.CodigoBanco == codigo);
        }

        private Banco BancoExisteNome(string nome)
        {
            return _context.Banco.FirstOrDefault(b => b.Nome == nome);
        }
    }
}