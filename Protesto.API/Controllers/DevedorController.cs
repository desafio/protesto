﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Protesto.BLL.Models;
using Protesto.DAL;

namespace Protesto.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevedorController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DevedorController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/devedor
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Devedor>>> GetDevedores()
        {
            return await _context.Devedor.ToListAsync();
        }

        // GET: api/devedor/5 
        [HttpGet("{id}")]
        public async Task<ActionResult<Devedor>> GetDevedor(int id)
        {
            var user = await _context.Devedor.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return user;
        }

        // POST api/devedor
        [HttpPost]
        public async Task<ActionResult<Devedor>> PostDevedor(Devedor devedor)
        {
            // Verifica se o cpf do devedor já existe
            Devedor devedorCad = DevedorExisteCpf(devedor.Cpf);
            if (devedorCad != null)
            {
                if (devedorCad.DevedorId > 0)
                {
                    return CreatedAtAction("GetDevedor", new { id = devedorCad.DevedorId }, devedorCad);
                }
            }

            _context.Devedor.Add(devedor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDevedor", new { id = devedor.DevedorId }, devedor);
        }

        // PUT api/devedor/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDevedor(int id, Devedor devedor)
        {
            if (id != devedor.DevedorId)
            {
                return BadRequest();
            }

            if (DevedorExiste(id))
            {
                _context.Entry(devedor).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetDevedor", new { id = devedor.DevedorId }, devedor);
                }
                catch (DbUpdateConcurrencyException)
                {
                    return BadRequest();
                }
            }
            else
            {
                return NotFound();
            }
        }

        // DELETE api/devedor/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Devedor>> DeleteDevedor(int id)
        {
            var devedor = await _context.Devedor.FindAsync(id);
            if (devedor == null)
            {
                return NotFound();
            }
            _context.Devedor.Remove(devedor);
            await _context.SaveChangesAsync();
            return devedor;
        }

        private bool DevedorExiste(int id)
        {
            return _context.Devedor.Any(e => e.DevedorId == id);
        }

        private Devedor DevedorExisteCpf(string cpf)
        {
            return _context.Devedor.FirstOrDefault(b => b.Cpf == cpf);
        }
    }
}