﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Protesto.BLL.Models;
using Protesto.DAL;

namespace Protesto.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TituloController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TituloController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/titulo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Titulo>>> GetTituloes()
        {
            return await _context.Titulo.ToListAsync();
        }

        // GET: api/titulo/5 
        [HttpGet("{id}")]
        public async Task<ActionResult<Titulo>> GetTitulo(int id)
        {
            var user = await _context.Titulo.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return user;
        }

        // POST api/titulo
        [HttpPost]
        public async Task<ActionResult<Titulo>> PostTitulo(Titulo titulo)
        {
            // Verifica se o cpf do titulo já existe
            Titulo tituloCad = TituloExisteCodigoInterno(titulo.CodigoInterno);
            if (tituloCad != null)
            {
                if (tituloCad.TituloId > 0)
                {
                    return CreatedAtAction("GetTitulo", new { id = tituloCad.TituloId }, tituloCad);
                }
            }

            _context.Titulo.Add(titulo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTitulo", new { id = titulo.TituloId }, titulo);
        }

        // PUT api/titulo/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTitulo(int id, Titulo titulo)
        {
            if (id != titulo.TituloId)
            {
                return BadRequest();
            }

            if (TituloExiste(id))
            {
                _context.Entry(titulo).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetTitulo", new { id = titulo.TituloId }, titulo);
                }
                catch (DbUpdateConcurrencyException)
                {
                    return BadRequest();
                }
            }
            else
            {
                return NotFound();
            }
        }

        // DELETE api/titulo/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Titulo>> DeleteTitulo(int id)
        {
            var titulo = await _context.Titulo.FindAsync(id);
            if (titulo == null)
            {
                return NotFound();
            }
            _context.Titulo.Remove(titulo);
            await _context.SaveChangesAsync();
            return titulo;
        }

        private bool TituloExiste(int id)
        {
            return _context.Titulo.Any(e => e.TituloId == id);
        }

        private Titulo TituloExisteCodigoInterno(string codigoInterno)
        {
            return _context.Titulo.FirstOrDefault(b => b.CodigoInterno == codigoInterno);
        }
    }
}