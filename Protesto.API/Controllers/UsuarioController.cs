﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Protesto.BLL.Models;
using Protesto.DAL;

namespace Protesto.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UsuarioController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/usuario
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Usuario>>> GetUsuarios()
        {
            return await _context.Usuario.ToListAsync();
        }

        // GET: api/usuario/5 
        [HttpGet("{id}")]
        public async Task<ActionResult<Usuario>> GetUsuario(int id)
        {
            var user = await _context.Usuario.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            return user;
        }

        // GET api/usuario/login
        [HttpPost("Login", Name = "Login")]
        [Route("/login")]
        public IActionResult LoginUsuario(Usuario usuario)
        {
            Usuario usuarioCad = LoginValido(usuario.Login, usuario.Senha);
            if (usuarioCad != null)
            {
                if (usuarioCad.UsuarioId > 0)
                {
                    return Ok(usuarioCad);
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return NotFound();
            }
        }

        // POST api/usuario
        [HttpPost]
        public async Task<ActionResult<Usuario>> PostUsuario(Usuario usuario)
        {
            // Verifica se o login já existe
            Usuario usuarioCad = UsuarioExisteLogin(usuario.Login);
            if (usuarioCad != null)
            {
                if (usuarioCad.UsuarioId > 0)
                {
                    return CreatedAtAction("GetUsuario", new { id = usuarioCad.UsuarioId }, usuarioCad);
                }
            }

            usuario.Senha = CriptografaSenha(usuario.Senha);
            _context.Usuario.Add(usuario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsuario", new { id = usuario.UsuarioId }, usuario);
        }

        // PUT api/usuario/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuario(int id, Usuario usuario)
        {
            if (id != usuario.UsuarioId)
            {
                return BadRequest();
            }

            if (UsuarioExiste(id))
            {
                _context.Entry(usuario).State = EntityState.Modified;

                try
                {
                    usuario.Senha = CriptografaSenha(usuario.Senha);
                    await _context.SaveChangesAsync();
                    return CreatedAtAction("GetUsuario", new { id = usuario.UsuarioId }, usuario);
                }
                catch (DbUpdateConcurrencyException)
                {
                    return BadRequest();
                }
            }
            else
            {
                return NotFound();
            }
        }

        // DELETE api/usuario/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Usuario>> DeleteUsuario(int id)
        {
            var usuario = await _context.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }
            _context.Usuario.Remove(usuario);
            await _context.SaveChangesAsync();
            return usuario;
        }

        private bool UsuarioExiste(int id)
        {
            return _context.Usuario.Any(e => e.UsuarioId == id);
        }

        private Usuario UsuarioExisteLogin(string login)
        {
            return _context.Usuario.FirstOrDefault(u => u.Login == login);
        }

        private Usuario LoginValido(string login, string senha)
        {
            return _context.Usuario.FirstOrDefault(e => e.Login == login && e.Senha == CriptografaSenha(senha));
        }

        private string CriptografaSenha(string senha)
        {
            try
            {
                MD5 md5 = MD5.Create();
                byte[] inputBytes = Encoding.ASCII.GetBytes(senha);
                byte[] hash = md5.ComputeHash(inputBytes);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}