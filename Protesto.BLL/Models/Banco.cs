﻿using System.ComponentModel.DataAnnotations;

namespace Protesto.BLL.Models
{
    public class Banco
    {
        [Key]
        public int BancoId { get; set; }
        public int CodigoBanco { get; set; }
        public string Nome { get; set; }
    }
}
