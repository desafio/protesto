﻿using System.ComponentModel.DataAnnotations;

namespace Protesto.BLL.Models
{
    public class Devedor
    {
        [Key]
        public int DevedorId { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Cep { get; set; }
        public string Uf { get; set; }
    }
}
