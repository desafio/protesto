﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Protesto.BLL.Models
{
    public class Titulo
    {
        [Key]
        public int TituloId { get; set; }

        [ForeignKey("Banco")]
        public int BancoId { get; set; }
        public Banco Banco { get; set; }

        [ForeignKey("Devedor")]
        public int DevedorId { get; set; }
        public Devedor Devedor { get; set; }

        public string CodigoInterno { get; set; }
        public int NumeroTitulo { get; set; }
        public int Parcela { get; set; }
        public string CidadePag { get; set; }
        public string UfPag { get; set; }
        public decimal ValorTitulo { get; set; }
        public decimal ValorProtestar { get; set; }
        public DateTime DataEmissao { get; set; }
        public DateTime DataVencimento { get; set; }
        public string TipoDocumento { get; set; }
        public string Operacao { get; set; }
        public decimal? ValorParcela { get; set; }
        public int? QtdeParcela { get; set; }
    }
}
