﻿using System.ComponentModel.DataAnnotations;

namespace Protesto.BLL.Models
{
    public class Usuario
    {
        [Key]
        public int UsuarioId { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
    }
}
