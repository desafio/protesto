﻿using Newtonsoft.Json;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Protesto.WIN.BLL.BO
{
    public class BancoBO
    {
        public List<Banco> listaBancos = null;
        public Banco banco = null;
        public bool resultado = false;

        private bool LogarAPI(string webApi, Banco banco)
        {
            using (var client = new HttpClient())
            {
                var serializedBanco = JsonConvert.SerializeObject(banco);
                var content = new StringContent(serializedBanco, Encoding.UTF8, "application/json");
                var result = client.PostAsync(webApi + "banco/login", content).Result;
                return result.IsSuccessStatusCode;
            }
        }

        public async Task<List<Banco>> GetBancosAsync(string webApi)
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetStringAsync(webApi + "banco");
                listaBancos = JsonConvert.DeserializeObject<List<Banco>>(response);
                return listaBancos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Banco> GetBancoAsync(string webApi, int bancoId)
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetStringAsync(webApi + "banco/" + bancoId);
                banco = JsonConvert.DeserializeObject<Banco>(response);
                return banco;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<bool> PostBancoAsync(string webApi, Banco banco)
        {
            using (var client = new HttpClient())
            {
                var serializedBanco = JsonConvert.SerializeObject(banco);
                var content = new StringContent(serializedBanco, Encoding.UTF8, "application/json");
                var result = await client.PostAsync(webApi + "banco", content);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
        }

        private async Task<bool> PutBancoAsync(string webApi, Banco banco)
        {
            using (var client = new HttpClient())
            {
                var serializedBanco = JsonConvert.SerializeObject(banco);
                var content = new StringContent(serializedBanco, Encoding.UTF8, "application/json");
                var result = await client.PutAsync(webApi + "banco/" + banco.BancoId, content);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
        }

        public async Task<bool> DeleteBancoAsync(string webApi, int bancoId)
        {
            try
            {
                HttpClient client = new HttpClient();
                var result = await client.DeleteAsync(webApi + "banco/" + bancoId);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ValidarSalvar(Banco banco)
        {
            string validacao = string.Empty;

            if (banco.CodigoBanco == 0)
            {
                validacao += "\r\nInforme o Código do Banco!";
            }

            if (string.IsNullOrEmpty(banco.Nome))
            {
                validacao += "\r\nInforme o Nome do Banco!";
            }

            return validacao;
        }

        public async Task<bool> SalvarBanco(string webApi, Banco banco)
        {
            try
            {
                string validacao = ValidarSalvar(banco);
                if (string.IsNullOrEmpty(validacao))
                {
                    if (banco.BancoId > 0)
                    {
                        resultado = await PutBancoAsync(webApi, banco);
                        return resultado;
                    }
                    else
                    {
                        resultado = await PostBancoAsync(webApi, banco);
                        return resultado;
                    }
                }
                else
                {
                    resultado = false;
                    throw new ArgumentException(validacao);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
