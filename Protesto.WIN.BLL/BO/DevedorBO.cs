﻿using Newtonsoft.Json;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Protesto.WIN.BLL.BO
{
    public class DevedorBO
    {
        public List<Devedor> listaDevedores = null;
        public Devedor devedor = null;
        public bool resultado = false;

        public async Task<List<Devedor>> GetDevedoresAsync(string webApi)
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetStringAsync(webApi + "devedor");
                listaDevedores = JsonConvert.DeserializeObject<List<Devedor>>(response);
                return listaDevedores;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Devedor> GetDevedorAsync(string webApi, int devedorId)
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetStringAsync(webApi + "devedor/" + devedorId);
                devedor = JsonConvert.DeserializeObject<Devedor>(response);
                return devedor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<bool> PostDevedorAsync(string webApi, Devedor devedor)
        {
            using (var client = new HttpClient())
            {
                var serializedDevedor = JsonConvert.SerializeObject(devedor);
                var content = new StringContent(serializedDevedor, Encoding.UTF8, "application/json");
                var result = await client.PostAsync(webApi + "devedor", content);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
        }

        private async Task<bool> PutDevedorAsync(string webApi, Devedor devedor)
        {
            using (var client = new HttpClient())
            {
                var serializedDevedor = JsonConvert.SerializeObject(devedor);
                var content = new StringContent(serializedDevedor, Encoding.UTF8, "application/json");
                var result = await client.PutAsync(webApi + "devedor/" + devedor.DevedorId, content);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
        }

        public async Task<bool> DeleteDevedorAsync(string webApi, int devedorId)
        {
            try
            {
                HttpClient client = new HttpClient();
                var result = await client.DeleteAsync(webApi + "devedor/" + devedorId);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ValidarSalvar(Devedor devedor)
        {
            string validacao = string.Empty;

            if (string.IsNullOrEmpty(devedor.Cpf))
            {
                validacao += "\r\nInforme o CPF do Devedor!";
            }

            if (string.IsNullOrEmpty(devedor.Nome))
            {
                validacao += "\r\nInforme o Nome do Devedor!";
            }

            return validacao;
        }

        public async Task<bool> SalvarDevedor(string webApi, Devedor devedor)
        {
            try
            {
                string validacao = ValidarSalvar(devedor);
                if (string.IsNullOrEmpty(validacao))
                {
                    if (devedor.DevedorId > 0)
                    {
                        resultado = await PutDevedorAsync(webApi, devedor);
                        return resultado;
                    }
                    else
                    {
                        resultado = await PostDevedorAsync(webApi, devedor);
                        return resultado;
                    }
                }
                else
                {
                    resultado = false;
                    throw new ArgumentException(validacao);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
