﻿using Newtonsoft.Json;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Protesto.WIN.BLL.BO
{
    public class TituloBO
    {
        public List<Titulo> listaTitulos = null;
        public Titulo titulo = null;
        public bool resultado = false;

        public async Task<List<Titulo>> GetTitulosAsync(string webApi)
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetStringAsync(webApi + "titulo");
                listaTitulos = JsonConvert.DeserializeObject<List<Titulo>>(response);
                return listaTitulos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Titulo> GetTituloAsync(string webApi, int tituloId)
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetStringAsync(webApi + "titulo/" + tituloId);
                titulo = JsonConvert.DeserializeObject<Titulo>(response);
                return titulo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<bool> PostTituloAsync(string webApi, Titulo titulo)
        {
            using (var client = new HttpClient())
            {
                var serializedTitulo = JsonConvert.SerializeObject(titulo);
                var content = new StringContent(serializedTitulo, Encoding.UTF8, "application/json");
                var result = await client.PostAsync(webApi + "titulo", content);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
        }

        private async Task<bool> PutTituloAsync(string webApi, Titulo titulo)
        {
            using (var client = new HttpClient())
            {
                var serializedTitulo = JsonConvert.SerializeObject(titulo);
                var content = new StringContent(serializedTitulo, Encoding.UTF8, "application/json");
                var result = await client.PutAsync(webApi + "titulo/" + titulo.TituloId, content);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
        }

        public async Task<bool> DeleteTituloAsync(string webApi, int tituloId)
        {
            try
            {
                HttpClient client = new HttpClient();
                var result = await client.DeleteAsync(webApi + "titulo/" + tituloId);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ValidarSalvar(Titulo titulo)
        {
            string validacao = string.Empty;

            if (string.IsNullOrEmpty(titulo.CodigoInterno))
            {
                validacao += "\r\nInforme o Código Interno do Titulo!";
            }

            if (string.IsNullOrEmpty(titulo.CidadePag))
            {
                validacao += "\r\nInforme a Cidade de Pagamento do Titulo!";
            }

            return validacao;
        }

        public async Task<bool> SalvarTitulo(string webApi, Titulo titulo)
        {
            try
            {
                string validacao = ValidarSalvar(titulo);
                if (string.IsNullOrEmpty(validacao))
                {
                    if (titulo.TituloId > 0)
                    {
                        resultado = await PutTituloAsync(webApi, titulo);
                        return resultado;
                    }
                    else
                    {
                        resultado = await PostTituloAsync(webApi, titulo);
                        return resultado;
                    }
                }
                else
                {
                    resultado = false;
                    throw new ArgumentException(validacao);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
