﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Protesto.WIN.BLL.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Protesto.WIN.BLL.BO
{
    public class UsuarioBO
    {
        public List<Usuario> listaUsuarios = null;
        public Usuario usuario = null;
        public bool resultado = false;

        private bool LogarAPI(string webApi, Usuario usuario)
        {
            using (var client = new HttpClient())
            {
                var serializedUsuario = JsonConvert.SerializeObject(usuario);
                var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                var result = client.PostAsync(webApi + "usuario/login", content).Result;
                return result.IsSuccessStatusCode;
            }
        }

        public async Task<List<Usuario>> GetUsuariosAsync(string webApi)
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetStringAsync(webApi + "usuario");
                listaUsuarios = JsonConvert.DeserializeObject<List<Usuario>>(response);
                return listaUsuarios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Usuario> GetUsuarioAsync(string webApi, int usuarioId)
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetStringAsync(webApi + "usuario/" + usuarioId);
                usuario = JsonConvert.DeserializeObject<Usuario>(response);
                return usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<bool> PostUsuarioAsync(string webApi, Usuario usuario)
        {
            using (var client = new HttpClient())
            {
                var serializedUsuario = JsonConvert.SerializeObject(usuario);
                var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                var result = await client.PostAsync(webApi + "usuario", content);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }           
        }

        private async Task<bool> PutUsuarioAsync(string webApi, Usuario usuario)
        {
            using (var client = new HttpClient())
            {
                var serializedUsuario = JsonConvert.SerializeObject(usuario);
                var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                var result = await client.PutAsync(webApi + "usuario/" + usuario.UsuarioId, content);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
        }

        public async Task<bool> DeleteUsuarioAsync(string webApi, int usuarioId)
        {
            try
            {
                HttpClient client = new HttpClient();
                var result = await client.DeleteAsync(webApi + "usuario/" + usuarioId);
                resultado = result.IsSuccessStatusCode;
                return resultado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ValidarLogar(Usuario usuario)
        {
            string validacao = string.Empty;

            if (string.IsNullOrEmpty(usuario.Login))
            {
                validacao += "\r\nInforme o Login!";
            }

            if (string.IsNullOrEmpty(usuario.Senha))
            {
                validacao += "\r\nInforme a Senha!";
            }

            return validacao;
        }

        public bool Logar(string webApi, Usuario usuario)
        {
            try
            {
                string validacao = ValidarLogar(usuario);
                if (string.IsNullOrEmpty(validacao))
                {
                    return LogarAPI(webApi, usuario);
                }
                else
                {
                    throw new ArgumentException(validacao);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> SalvarUsuario(string webApi, Usuario usuario)
        {
            try
            {
                if (usuario.UsuarioId > 0)
                {
                    resultado = await PutUsuarioAsync(webApi, usuario);
                    return resultado;
                }
                else
                {
                    resultado = await PostUsuarioAsync(webApi, usuario);
                    return resultado;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
