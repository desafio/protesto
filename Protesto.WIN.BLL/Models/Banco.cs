﻿namespace Protesto.WIN.BLL.Models
{
    public class Banco
    {
        public int BancoId { get; set; }
        public int CodigoBanco { get; set; }
        public string Nome { get; set; }
    }
}
