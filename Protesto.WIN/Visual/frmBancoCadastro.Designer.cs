﻿namespace Protesto.WIN.Visual
{
    partial class frmBancoCadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpCadastro = new System.Windows.Forms.TableLayoutPanel();
            this.pnlCadastro = new System.Windows.Forms.Panel();
            this.lblBancoId = new System.Windows.Forms.Label();
            this.txtBancoId = new System.Windows.Forms.TextBox();
            this.lblNomeBanco = new System.Windows.Forms.Label();
            this.txtNomeBanco = new System.Windows.Forms.TextBox();
            this.lblCodigoBanco = new System.Windows.Forms.Label();
            this.txtCodigoBanco = new System.Windows.Forms.TextBox();
            this.pnlBotao = new System.Windows.Forms.Panel();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.tlpCadastro.SuspendLayout();
            this.pnlCadastro.SuspendLayout();
            this.pnlBotao.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpCadastro
            // 
            this.tlpCadastro.ColumnCount = 1;
            this.tlpCadastro.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCadastro.Controls.Add(this.pnlCadastro, 0, 0);
            this.tlpCadastro.Controls.Add(this.pnlBotao, 0, 1);
            this.tlpCadastro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCadastro.Location = new System.Drawing.Point(0, 0);
            this.tlpCadastro.Name = "tlpCadastro";
            this.tlpCadastro.RowCount = 2;
            this.tlpCadastro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCadastro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tlpCadastro.Size = new System.Drawing.Size(498, 140);
            this.tlpCadastro.TabIndex = 0;
            // 
            // pnlCadastro
            // 
            this.pnlCadastro.Controls.Add(this.lblBancoId);
            this.pnlCadastro.Controls.Add(this.txtBancoId);
            this.pnlCadastro.Controls.Add(this.lblNomeBanco);
            this.pnlCadastro.Controls.Add(this.txtNomeBanco);
            this.pnlCadastro.Controls.Add(this.lblCodigoBanco);
            this.pnlCadastro.Controls.Add(this.txtCodigoBanco);
            this.pnlCadastro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCadastro.Location = new System.Drawing.Point(3, 3);
            this.pnlCadastro.Name = "pnlCadastro";
            this.pnlCadastro.Size = new System.Drawing.Size(492, 91);
            this.pnlCadastro.TabIndex = 0;
            // 
            // lblBancoId
            // 
            this.lblBancoId.AutoSize = true;
            this.lblBancoId.Location = new System.Drawing.Point(98, 11);
            this.lblBancoId.Name = "lblBancoId";
            this.lblBancoId.Size = new System.Drawing.Size(21, 13);
            this.lblBancoId.TabIndex = 18;
            this.lblBancoId.Text = "ID:";
            // 
            // txtBancoId
            // 
            this.txtBancoId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBancoId.Location = new System.Drawing.Point(125, 9);
            this.txtBancoId.MaxLength = 50;
            this.txtBancoId.Name = "txtBancoId";
            this.txtBancoId.ReadOnly = true;
            this.txtBancoId.Size = new System.Drawing.Size(161, 20);
            this.txtBancoId.TabIndex = 17;
            this.txtBancoId.TabStop = false;
            // 
            // lblNomeBanco
            // 
            this.lblNomeBanco.AutoSize = true;
            this.lblNomeBanco.Location = new System.Drawing.Point(25, 63);
            this.lblNomeBanco.Name = "lblNomeBanco";
            this.lblNomeBanco.Size = new System.Drawing.Size(94, 13);
            this.lblNomeBanco.TabIndex = 13;
            this.lblNomeBanco.Text = "* Nome do Banco:";
            // 
            // txtNomeBanco
            // 
            this.txtNomeBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomeBanco.Location = new System.Drawing.Point(125, 61);
            this.txtNomeBanco.MaxLength = 100;
            this.txtNomeBanco.Name = "txtNomeBanco";
            this.txtNomeBanco.Size = new System.Drawing.Size(351, 20);
            this.txtNomeBanco.TabIndex = 12;
            // 
            // lblCodigoBanco
            // 
            this.lblCodigoBanco.AutoSize = true;
            this.lblCodigoBanco.Location = new System.Drawing.Point(20, 37);
            this.lblCodigoBanco.Name = "lblCodigoBanco";
            this.lblCodigoBanco.Size = new System.Drawing.Size(99, 13);
            this.lblCodigoBanco.TabIndex = 16;
            this.lblCodigoBanco.Text = "* Código do Banco:";
            // 
            // txtCodigoBanco
            // 
            this.txtCodigoBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodigoBanco.Location = new System.Drawing.Point(125, 35);
            this.txtCodigoBanco.MaxLength = 8;
            this.txtCodigoBanco.Name = "txtCodigoBanco";
            this.txtCodigoBanco.Size = new System.Drawing.Size(161, 20);
            this.txtCodigoBanco.TabIndex = 9;
            this.txtCodigoBanco.Leave += new System.EventHandler(this.txtCodigoBanco_Leave);
            // 
            // pnlBotao
            // 
            this.pnlBotao.Controls.Add(this.btnExcluir);
            this.pnlBotao.Controls.Add(this.btnCancelar);
            this.pnlBotao.Controls.Add(this.btnSalvar);
            this.pnlBotao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBotao.Location = new System.Drawing.Point(3, 100);
            this.pnlBotao.Name = "pnlBotao";
            this.pnlBotao.Size = new System.Drawing.Size(492, 37);
            this.pnlBotao.TabIndex = 1;
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::Protesto.WIN.Properties.Resources.deletar_mdpi_cinza;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(167, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(159, 31);
            this.btnExcluir.TabIndex = 14;
            this.btnExcluir.Text = "Excluir Banco";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::Protesto.WIN.Properties.Resources.cancelar_mdpi_cinza;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(332, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(155, 31);
            this.btnCancelar.TabIndex = 15;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Image = global::Protesto.WIN.Properties.Resources.salvar_mdpi_cinza;
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalvar.Location = new System.Drawing.Point(3, 3);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(158, 31);
            this.btnSalvar.TabIndex = 13;
            this.btnSalvar.Text = "Salvar Banco";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // frmBancoCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 140);
            this.Controls.Add(this.tlpCadastro);
            this.MaximizeBox = false;
            this.Name = "frmBancoCadastro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmBancoCadastro";
            this.tlpCadastro.ResumeLayout(false);
            this.pnlCadastro.ResumeLayout(false);
            this.pnlCadastro.PerformLayout();
            this.pnlBotao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpCadastro;
        private System.Windows.Forms.Panel pnlCadastro;
        private System.Windows.Forms.Label lblNomeBanco;
        private System.Windows.Forms.TextBox txtNomeBanco;
        private System.Windows.Forms.Label lblCodigoBanco;
        private System.Windows.Forms.TextBox txtCodigoBanco;
        private System.Windows.Forms.Panel pnlBotao;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Label lblBancoId;
        private System.Windows.Forms.TextBox txtBancoId;
        private System.Windows.Forms.Button btnExcluir;
    }
}