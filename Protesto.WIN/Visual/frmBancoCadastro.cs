﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmBancoCadastro : Form
    {
        public frmBancoCadastro()
        {
            InitializeComponent();
            txtCodigoBanco.Enabled = true;
            this.Text = "Cadastrar Novo Banco";
        }

        public frmBancoCadastro(int usuarioId)
        {
            InitializeComponent();
            txtCodigoBanco.Enabled = false;
            ExibirInformacoes(usuarioId);
            this.Text = "Editar Banco Cadastrado";
        }

        #region métodos

        private async void ExibirInformacoes(int usuarioId)
        {
            BancoBO bancoBO = new BancoBO();
            await bancoBO.GetBancoAsync(Program.API_URL, usuarioId);

            if (bancoBO.banco != null)
            {
                txtBancoId.Text = bancoBO.banco.BancoId.ToString();
                txtCodigoBanco.Text = bancoBO.banco.CodigoBanco.ToString();
                txtNomeBanco.Text = bancoBO.banco.Nome.ToString();
            }
        }

        #endregion

        #region eventos

        private async void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Banco banco = new Banco
                {
                    BancoId = string.IsNullOrEmpty(txtBancoId.Text) ? 0 : Convert.ToInt32(txtBancoId.Text),
                    CodigoBanco = string.IsNullOrEmpty(txtCodigoBanco.Text) ? 0 : Convert.ToInt32(txtCodigoBanco.Text),
                    Nome = txtNomeBanco.Text.Trim()
                };

                BancoBO bancoBO = new BancoBO();
                await bancoBO.SalvarBanco(Program.API_URL, banco);

                if (bancoBO.resultado)
                {
                    MessageBox.Show("Dados salvos com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Não foi possível salvar os dados, operação cancelada!", "Operação Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtCodigoBanco_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCodigoBanco.Text))
            {
                if (!int.TryParse(txtCodigoBanco.Text, out int codigo))
                {
                    MessageBox.Show("O código do banco deve conter apenas números!");
                    txtCodigoBanco.Clear();
                    txtCodigoBanco.Focus();
                }
            }
        }

        private async void btnExcluir_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Confirma a exclusão?", "Confirmação?", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes)
            {
                return;
            }

            int bancoId = string.IsNullOrEmpty(txtBancoId.Text) ? 0 : Convert.ToInt32(txtBancoId.Text);

            BancoBO bancoBO = new BancoBO();
            await bancoBO.DeleteBancoAsync(Program.API_URL, bancoId);

            if (bancoBO.resultado)
            {
                MessageBox.Show("Dados excluídos com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            else
            {
                MessageBox.Show("Não foi possível excluir os dados, operação cancelada!", "Operação Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }


        #endregion
    }
}
