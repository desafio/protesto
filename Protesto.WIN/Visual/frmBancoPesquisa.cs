﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmBancoPesquisa : Form
    {
        public frmBancoPesquisa()
        {
            InitializeComponent();
            PopularGridBancos();
        }

        #region Métodos


        private async void PopularGridBancos()
        {
            lblTotal.Text = "* * *";

            //popular grid
            BancoBO bancoBO = new BancoBO();
            await bancoBO.GetBancosAsync(Program.API_URL);

            dgvDados.DataSource = bancoBO.listaBancos;
            PersonalizarGridBancos();
            lblTotal.Text = $"Bancos: {dgvDados.RowCount}";
        }

        private void PersonalizarGridBancos()
        {
            //titulo                       
            dgvDados.Columns["bancoId"].HeaderText = "Banco Id";
            dgvDados.Columns["codigoBanco"].HeaderText = "Código do Banco";
            dgvDados.Columns["Nome"].HeaderText = "Nome";            

            //alinhar
            foreach (DataGridViewColumn column in dgvDados.Columns)
            {
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                if (column.DataPropertyName == "bancoId")
                {
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
            }

            // criar link
            dgvDados.Columns["bancoId"].DefaultCellStyle.Font = new Font(Font, FontStyle.Underline);
            dgvDados.Columns["bancoId"].DefaultCellStyle.ForeColor = Color.Blue;
            dgvDados.Columns["bancoId"].DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
        }

        #endregion

        private void dgvBancos_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex == dgvDados.Columns["bancoId"].Index)
            {
                dgvDados.Cursor = Cursors.Hand;
            }
            else
            {
                dgvDados.Cursor = Cursors.Arrow;
            }
        }

        private void dgvBancos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //EDITAR REGISTRO DA GRID
            if (e.ColumnIndex == dgvDados.Columns["bancoId"].Index && dgvDados.Cursor == Cursors.Hand)
            {
                int bancoId = Convert.ToInt32(dgvDados.Rows[e.RowIndex].Cells["bancoId"].Value.ToString());

                frmBancoCadastro cad = new frmBancoCadastro(bancoId);
                cad.ShowDialog();

                PopularGridBancos();
            }
        }

        private void dgvDados_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex == dgvDados.Columns["bancoId"].Index)
            {
                dgvDados.Cursor = Cursors.Hand;
            }
            else
            {
                dgvDados.Cursor = Cursors.Arrow;
            }
        }

        private void dgvDados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //EDITAR REGISTRO DA GRID
            if (e.ColumnIndex == dgvDados.Columns["bancoId"].Index && dgvDados.Cursor == Cursors.Hand)
            {
                int bancoId = Convert.ToInt32(dgvDados.Rows[e.RowIndex].Cells["bancoId"].Value.ToString());

                frmBancoCadastro cad = new frmBancoCadastro(bancoId);
                cad.ShowDialog();

                PopularGridBancos();
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmBancoCadastro cad = new frmBancoCadastro();
            cad.ShowDialog();

            PopularGridBancos();
        }
    }
}
