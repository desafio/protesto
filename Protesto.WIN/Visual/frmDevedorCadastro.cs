﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmDevedorCadastro : Form
    {
        public frmDevedorCadastro()
        {
            InitializeComponent();
            txtCpf.Enabled = true;
            this.Text = "Cadastrar Novo Devedor";
        }

        public frmDevedorCadastro(int usuarioId)
        {
            InitializeComponent();
            txtCpf.Enabled = false;
            ExibirInformacoes(usuarioId);
            this.Text = "Editar Devedor Cadastrado";
        }

        #region métodos

        private async void ExibirInformacoes(int usuarioId)
        {
            DevedorBO devedorBO = new DevedorBO();
            await devedorBO.GetDevedorAsync(Program.API_URL, usuarioId);

            if (devedorBO.devedor != null)
            {
                txtDevedorId.Text = devedorBO.devedor.DevedorId.ToString();
                txtCpf.Text = devedorBO.devedor.Cpf.ToString();
                txtNome.Text = devedorBO.devedor.Nome.ToString();
                txtCep.Text = devedorBO.devedor.Cep.ToString();
                txtEndereco.Text = devedorBO.devedor.Endereco.ToString();
                txtBairro.Text = devedorBO.devedor.Bairro.ToString();
                txtCidade.Text = devedorBO.devedor.Cidade.ToString();
                txtUF.Text = devedorBO.devedor.Uf.ToString();
            }
        }

        #endregion

        #region eventos

        private void txtCPF_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCpf.Text))
            {
                if (!long.TryParse(txtCpf.Text, out long cpf))
                {
                    MessageBox.Show("O CPF deve conter apenas números!");
                    txtCpf.Clear();
                    txtCpf.Focus();
                }
                else
                {
                    if (!UtilBO.ValidaCpf(txtCpf.Text.Trim()))
                    {
                        MessageBox.Show("CPF é inválido!");
                        txtCpf.Clear();
                        txtCpf.Focus();
                    }
                }
            }
        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCep.Text))
            {
                if (!int.TryParse(txtCep.Text, out int codigo))
                {
                    MessageBox.Show("O CEP deve conter apenas números!");
                    txtCep.Clear();
                    txtCep.Focus();
                }
            }
        }

        private async void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Devedor devedor = new Devedor
                {
                    DevedorId = string.IsNullOrEmpty(txtDevedorId.Text) ? 0 : Convert.ToInt32(txtDevedorId.Text),
                    Cpf = txtCpf.Text.Trim(),
                    Nome = txtNome.Text.Trim(),
                    Cep = txtCep.Text.Trim(),
                    Endereco = txtEndereco.Text.Trim(),
                    Bairro = txtBairro.Text.Trim(),
                    Cidade = txtCidade.Text.Trim(),
                    Uf = txtUF.Text.Trim()                    
                };

                DevedorBO devedorBO = new DevedorBO();
                await devedorBO.SalvarDevedor(Program.API_URL, devedor);

                if (devedorBO.resultado)
                {
                    MessageBox.Show("Dados salvos com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Não foi possível salvar os dados, operação cancelada!", "Operação Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void btnExcluir_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Confirma a exclusão?", "Confirmação?", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes)
            {
                return;
            }

            int devedorId = string.IsNullOrEmpty(txtDevedorId.Text) ? 0 : Convert.ToInt32(txtDevedorId.Text);

            DevedorBO devedorBO = new DevedorBO();
            await devedorBO.DeleteDevedorAsync(Program.API_URL, devedorId);

            if (devedorBO.resultado)
            {
                MessageBox.Show("Dados excluídos com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            else
            {
                MessageBox.Show("Não foi possível excluir os dados, operação cancelada!", "Operação Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }


        #endregion
    }
}
