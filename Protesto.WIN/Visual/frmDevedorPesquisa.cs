﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmDevedorPesquisa : Form
    {
        public frmDevedorPesquisa()
        {
            InitializeComponent();
            PopularGridDevedores();
        }

        #region Métodos


        private async void PopularGridDevedores()
        {
            lblTotal.Text = "* * *";

            //popular grid
            DevedorBO devedorBO = new DevedorBO();
            await devedorBO.GetDevedoresAsync(Program.API_URL);

            dgvDados.DataSource = devedorBO.listaDevedores;
            PersonalizarGridDevedores();
            lblTotal.Text = $"Devedores: {dgvDados.RowCount}";
        }

        private void PersonalizarGridDevedores()
        {
            //titulo                       
            dgvDados.Columns["devedorId"].HeaderText = "Devedor Id";
            dgvDados.Columns["cpf"].HeaderText = "CPF";
            dgvDados.Columns["nome"].HeaderText = "Nome";
            dgvDados.Columns["cep"].HeaderText = "CEP";
            dgvDados.Columns["endereco"].HeaderText = "Endereço";
            dgvDados.Columns["bairro"].HeaderText = "Bairro";
            dgvDados.Columns["cidade"].HeaderText = "Cidade";
            dgvDados.Columns["uf"].HeaderText = "UF";

            //alinhar
            foreach (DataGridViewColumn column in dgvDados.Columns)
            {
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                if (column.DataPropertyName == "devedorId")
                {
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
            }

            // criar link
            dgvDados.Columns["devedorId"].DefaultCellStyle.Font = new Font(Font, FontStyle.Underline);
            dgvDados.Columns["devedorId"].DefaultCellStyle.ForeColor = Color.Blue;
            dgvDados.Columns["devedorId"].DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
        }

        #endregion

        private void dgvDevedores_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex == dgvDados.Columns["devedorId"].Index)
            {
                dgvDados.Cursor = Cursors.Hand;
            }
            else
            {
                dgvDados.Cursor = Cursors.Arrow;
            }
        }

        private void dgvDevedores_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //EDITAR REGISTRO DA GRID
            if (e.ColumnIndex == dgvDados.Columns["devedorId"].Index && dgvDados.Cursor == Cursors.Hand)
            {
                int devedorId = Convert.ToInt32(dgvDados.Rows[e.RowIndex].Cells["devedorId"].Value.ToString());

                frmDevedorCadastro cad = new frmDevedorCadastro(devedorId);
                cad.ShowDialog();

                PopularGridDevedores();
            }
        }

        private void dgvDados_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex == dgvDados.Columns["devedorId"].Index)
            {
                dgvDados.Cursor = Cursors.Hand;
            }
            else
            {
                dgvDados.Cursor = Cursors.Arrow;
            }
        }

        private void dgvDados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //EDITAR REGISTRO DA GRID
            if (e.ColumnIndex == dgvDados.Columns["devedorId"].Index && dgvDados.Cursor == Cursors.Hand)
            {
                int devedorId = Convert.ToInt32(dgvDados.Rows[e.RowIndex].Cells["devedorId"].Value.ToString());

                frmDevedorCadastro cad = new frmDevedorCadastro(devedorId);
                cad.ShowDialog();

                PopularGridDevedores();
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmDevedorCadastro cad = new frmDevedorCadastro();
            cad.ShowDialog();

            PopularGridDevedores();
        }
    }
}
