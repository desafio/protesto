﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Protesto.WIN.Visual;

namespace Protesto.WIN.Visual
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLogar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = new Usuario()
                {
                    Login = txtLogin.Text,
                    Senha = txtSenha.Text
                };

                if (new UsuarioBO().Logar(Program.API_URL, usuario))
                {
                    frmMenu menu = new frmMenu(usuario.Login);
                    menu.Show();

                    Hide();
                }
                else
                {
                    MessageBox.Show("Usuário e/ou senha inválidos!");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
