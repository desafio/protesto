﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        public frmMenu(string login)
        {
            InitializeComponent();

            lblUsuarioLogado.Text = "Usuário Logado: " + login;
        }

        private void frmMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void usuárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuarioPesquisa pesq = new frmUsuarioPesquisa();
            pesq.WindowState = FormWindowState.Maximized;
            pesq.ShowDialog();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmBancoPesquisa pesq = new frmBancoPesquisa();
            pesq.WindowState = FormWindowState.Maximized;
            pesq.ShowDialog();
        }

        private void devedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDevedorPesquisa pesq = new frmDevedorPesquisa();
            pesq.WindowState = FormWindowState.Maximized;
            pesq.ShowDialog();
        }

        private void títulosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTituloPesquisa pesq = new frmTituloPesquisa();
            pesq.WindowState = FormWindowState.Maximized;
            pesq.ShowDialog();
        }

        private void cargaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTituloCarga carga = new frmTituloCarga();
            carga.WindowState = FormWindowState.Maximized;
            carga.ShowDialog();
        }
    }
}
