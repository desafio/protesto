﻿namespace Protesto.WIN.Visual
{
    partial class frmTituloCadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpCadastro = new System.Windows.Forms.TableLayoutPanel();
            this.pnlCadastro = new System.Windows.Forms.Panel();
            this.lblUFPag = new System.Windows.Forms.Label();
            this.txtUFPag = new System.Windows.Forms.TextBox();
            this.lblCidadePag = new System.Windows.Forms.Label();
            this.txtCidadePag = new System.Windows.Forms.TextBox();
            this.lblBancoId = new System.Windows.Forms.Label();
            this.txtTituloId = new System.Windows.Forms.TextBox();
            this.lblCodigoInterno = new System.Windows.Forms.Label();
            this.txtCodigoInterno = new System.Windows.Forms.TextBox();
            this.pnlBotao = new System.Windows.Forms.Panel();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.cboBanco = new System.Windows.Forms.ComboBox();
            this.lblBanco = new System.Windows.Forms.Label();
            this.cboDevedor = new System.Windows.Forms.ComboBox();
            this.lblDevedor = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.txtTitulo = new System.Windows.Forms.TextBox();
            this.lblParcela = new System.Windows.Forms.Label();
            this.txtParcela = new System.Windows.Forms.TextBox();
            this.lblValorTitulo = new System.Windows.Forms.Label();
            this.txtValorTitulo = new System.Windows.Forms.TextBox();
            this.lblValorProtestar = new System.Windows.Forms.Label();
            this.txtValorProtestar = new System.Windows.Forms.TextBox();
            this.lblDataEmissao = new System.Windows.Forms.Label();
            this.mtbDataEmissao = new System.Windows.Forms.MaskedTextBox();
            this.lblDataVencimento = new System.Windows.Forms.Label();
            this.mtbDataVencimento = new System.Windows.Forms.MaskedTextBox();
            this.lblTipoDocumento = new System.Windows.Forms.Label();
            this.txtTipoDocumento = new System.Windows.Forms.TextBox();
            this.lblOperacao = new System.Windows.Forms.Label();
            this.txtOperacao = new System.Windows.Forms.TextBox();
            this.lblValorParcela = new System.Windows.Forms.Label();
            this.txtValorParcela = new System.Windows.Forms.TextBox();
            this.lblQtdeParcela = new System.Windows.Forms.Label();
            this.txtQtdeParcelas = new System.Windows.Forms.TextBox();
            this.tlpCadastro.SuspendLayout();
            this.pnlCadastro.SuspendLayout();
            this.pnlBotao.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpCadastro
            // 
            this.tlpCadastro.ColumnCount = 1;
            this.tlpCadastro.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCadastro.Controls.Add(this.pnlCadastro, 0, 0);
            this.tlpCadastro.Controls.Add(this.pnlBotao, 0, 1);
            this.tlpCadastro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCadastro.Location = new System.Drawing.Point(0, 0);
            this.tlpCadastro.Name = "tlpCadastro";
            this.tlpCadastro.RowCount = 2;
            this.tlpCadastro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCadastro.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tlpCadastro.Size = new System.Drawing.Size(498, 476);
            this.tlpCadastro.TabIndex = 0;
            // 
            // pnlCadastro
            // 
            this.pnlCadastro.Controls.Add(this.lblQtdeParcela);
            this.pnlCadastro.Controls.Add(this.txtQtdeParcelas);
            this.pnlCadastro.Controls.Add(this.lblValorParcela);
            this.pnlCadastro.Controls.Add(this.txtValorParcela);
            this.pnlCadastro.Controls.Add(this.lblOperacao);
            this.pnlCadastro.Controls.Add(this.txtOperacao);
            this.pnlCadastro.Controls.Add(this.lblTipoDocumento);
            this.pnlCadastro.Controls.Add(this.txtTipoDocumento);
            this.pnlCadastro.Controls.Add(this.lblDataVencimento);
            this.pnlCadastro.Controls.Add(this.mtbDataVencimento);
            this.pnlCadastro.Controls.Add(this.lblDataEmissao);
            this.pnlCadastro.Controls.Add(this.mtbDataEmissao);
            this.pnlCadastro.Controls.Add(this.lblValorProtestar);
            this.pnlCadastro.Controls.Add(this.txtValorProtestar);
            this.pnlCadastro.Controls.Add(this.lblValorTitulo);
            this.pnlCadastro.Controls.Add(this.txtValorTitulo);
            this.pnlCadastro.Controls.Add(this.lblParcela);
            this.pnlCadastro.Controls.Add(this.txtParcela);
            this.pnlCadastro.Controls.Add(this.lblTitulo);
            this.pnlCadastro.Controls.Add(this.txtTitulo);
            this.pnlCadastro.Controls.Add(this.cboDevedor);
            this.pnlCadastro.Controls.Add(this.lblDevedor);
            this.pnlCadastro.Controls.Add(this.cboBanco);
            this.pnlCadastro.Controls.Add(this.lblBanco);
            this.pnlCadastro.Controls.Add(this.lblUFPag);
            this.pnlCadastro.Controls.Add(this.txtUFPag);
            this.pnlCadastro.Controls.Add(this.lblCidadePag);
            this.pnlCadastro.Controls.Add(this.txtCidadePag);
            this.pnlCadastro.Controls.Add(this.lblBancoId);
            this.pnlCadastro.Controls.Add(this.txtTituloId);
            this.pnlCadastro.Controls.Add(this.lblCodigoInterno);
            this.pnlCadastro.Controls.Add(this.txtCodigoInterno);
            this.pnlCadastro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCadastro.Location = new System.Drawing.Point(3, 3);
            this.pnlCadastro.Name = "pnlCadastro";
            this.pnlCadastro.Size = new System.Drawing.Size(492, 427);
            this.pnlCadastro.TabIndex = 0;
            // 
            // lblUFPag
            // 
            this.lblUFPag.AutoSize = true;
            this.lblUFPag.Location = new System.Drawing.Point(26, 195);
            this.lblUFPag.Name = "lblUFPag";
            this.lblUFPag.Size = new System.Drawing.Size(103, 13);
            this.lblUFPag.TabIndex = 28;
            this.lblUFPag.Text = "* UF do Pagamento:";
            // 
            // txtUFPag
            // 
            this.txtUFPag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUFPag.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUFPag.Location = new System.Drawing.Point(135, 193);
            this.txtUFPag.MaxLength = 2;
            this.txtUFPag.Name = "txtUFPag";
            this.txtUFPag.Size = new System.Drawing.Size(86, 20);
            this.txtUFPag.TabIndex = 39;
            // 
            // lblCidadePag
            // 
            this.lblCidadePag.AutoSize = true;
            this.lblCidadePag.Location = new System.Drawing.Point(7, 169);
            this.lblCidadePag.Name = "lblCidadePag";
            this.lblCidadePag.Size = new System.Drawing.Size(122, 13);
            this.lblCidadePag.TabIndex = 26;
            this.lblCidadePag.Text = "* Cidade do Pagamento:";
            // 
            // txtCidadePag
            // 
            this.txtCidadePag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCidadePag.Location = new System.Drawing.Point(135, 167);
            this.txtCidadePag.MaxLength = 100;
            this.txtCidadePag.Name = "txtCidadePag";
            this.txtCidadePag.Size = new System.Drawing.Size(351, 20);
            this.txtCidadePag.TabIndex = 38;
            // 
            // lblBancoId
            // 
            this.lblBancoId.AutoSize = true;
            this.lblBancoId.Location = new System.Drawing.Point(108, 11);
            this.lblBancoId.Name = "lblBancoId";
            this.lblBancoId.Size = new System.Drawing.Size(21, 13);
            this.lblBancoId.TabIndex = 18;
            this.lblBancoId.Text = "ID:";
            // 
            // txtTituloId
            // 
            this.txtTituloId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTituloId.Location = new System.Drawing.Point(135, 9);
            this.txtTituloId.MaxLength = 50;
            this.txtTituloId.Name = "txtTituloId";
            this.txtTituloId.ReadOnly = true;
            this.txtTituloId.Size = new System.Drawing.Size(161, 20);
            this.txtTituloId.TabIndex = 17;
            this.txtTituloId.TabStop = false;
            // 
            // lblCodigoInterno
            // 
            this.lblCodigoInterno.AutoSize = true;
            this.lblCodigoInterno.Location = new System.Drawing.Point(43, 91);
            this.lblCodigoInterno.Name = "lblCodigoInterno";
            this.lblCodigoInterno.Size = new System.Drawing.Size(86, 13);
            this.lblCodigoInterno.TabIndex = 16;
            this.lblCodigoInterno.Text = "* Código Interno:";
            // 
            // txtCodigoInterno
            // 
            this.txtCodigoInterno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodigoInterno.Location = new System.Drawing.Point(135, 89);
            this.txtCodigoInterno.MaxLength = 20;
            this.txtCodigoInterno.Name = "txtCodigoInterno";
            this.txtCodigoInterno.Size = new System.Drawing.Size(161, 20);
            this.txtCodigoInterno.TabIndex = 32;
            this.txtCodigoInterno.Leave += new System.EventHandler(this.txtCodigoInterno_Leave);
            // 
            // pnlBotao
            // 
            this.pnlBotao.Controls.Add(this.btnExcluir);
            this.pnlBotao.Controls.Add(this.btnCancelar);
            this.pnlBotao.Controls.Add(this.btnSalvar);
            this.pnlBotao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBotao.Location = new System.Drawing.Point(3, 436);
            this.pnlBotao.Name = "pnlBotao";
            this.pnlBotao.Size = new System.Drawing.Size(492, 37);
            this.pnlBotao.TabIndex = 1;
            // 
            // btnExcluir
            // 
            this.btnExcluir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnExcluir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcluir.Image = global::Protesto.WIN.Properties.Resources.deletar_mdpi_cinza;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(167, 3);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(159, 31);
            this.btnExcluir.TabIndex = 19;
            this.btnExcluir.Text = "Excluir Título";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = global::Protesto.WIN.Properties.Resources.cancelar_mdpi_cinza;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(332, 3);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(155, 31);
            this.btnCancelar.TabIndex = 20;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Image = global::Protesto.WIN.Properties.Resources.salvar_mdpi_cinza;
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalvar.Location = new System.Drawing.Point(3, 3);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(158, 31);
            this.btnSalvar.TabIndex = 18;
            this.btnSalvar.Text = "Salvar Título";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // cboBanco
            // 
            this.cboBanco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboBanco.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboBanco.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboBanco.FormattingEnabled = true;
            this.cboBanco.Location = new System.Drawing.Point(135, 35);
            this.cboBanco.Name = "cboBanco";
            this.cboBanco.Size = new System.Drawing.Size(351, 21);
            this.cboBanco.TabIndex = 29;
            // 
            // lblBanco
            // 
            this.lblBanco.AutoSize = true;
            this.lblBanco.Location = new System.Drawing.Point(81, 38);
            this.lblBanco.Name = "lblBanco";
            this.lblBanco.Size = new System.Drawing.Size(48, 13);
            this.lblBanco.TabIndex = 30;
            this.lblBanco.Text = "* Banco:";
            // 
            // cboDevedor
            // 
            this.cboDevedor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDevedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboDevedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboDevedor.FormattingEnabled = true;
            this.cboDevedor.Location = new System.Drawing.Point(135, 62);
            this.cboDevedor.Name = "cboDevedor";
            this.cboDevedor.Size = new System.Drawing.Size(351, 21);
            this.cboDevedor.TabIndex = 31;
            // 
            // lblDevedor
            // 
            this.lblDevedor.AutoSize = true;
            this.lblDevedor.Location = new System.Drawing.Point(71, 65);
            this.lblDevedor.Name = "lblDevedor";
            this.lblDevedor.Size = new System.Drawing.Size(58, 13);
            this.lblDevedor.TabIndex = 32;
            this.lblDevedor.Text = "* Devedor:";
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Location = new System.Drawing.Point(29, 117);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(100, 13);
            this.lblTitulo.TabIndex = 33;
            this.lblTitulo.Text = "* Número do Título:";
            // 
            // txtTitulo
            // 
            this.txtTitulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTitulo.Location = new System.Drawing.Point(135, 115);
            this.txtTitulo.MaxLength = 8;
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.Size = new System.Drawing.Size(161, 20);
            this.txtTitulo.TabIndex = 34;
            this.txtTitulo.Leave += new System.EventHandler(this.txtTitulo_Leave);
            // 
            // lblParcela
            // 
            this.lblParcela.AutoSize = true;
            this.lblParcela.Location = new System.Drawing.Point(21, 143);
            this.lblParcela.Name = "lblParcela";
            this.lblParcela.Size = new System.Drawing.Size(108, 13);
            this.lblParcela.TabIndex = 35;
            this.lblParcela.Text = "* Número da Parcela:";
            // 
            // txtParcela
            // 
            this.txtParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtParcela.Location = new System.Drawing.Point(135, 141);
            this.txtParcela.MaxLength = 8;
            this.txtParcela.Name = "txtParcela";
            this.txtParcela.Size = new System.Drawing.Size(161, 20);
            this.txtParcela.TabIndex = 36;
            this.txtParcela.Leave += new System.EventHandler(this.txtParcela_Leave);
            // 
            // lblValorTitulo
            // 
            this.lblValorTitulo.AutoSize = true;
            this.lblValorTitulo.Location = new System.Drawing.Point(42, 221);
            this.lblValorTitulo.Name = "lblValorTitulo";
            this.lblValorTitulo.Size = new System.Drawing.Size(87, 13);
            this.lblValorTitulo.TabIndex = 39;
            this.lblValorTitulo.Text = "* Valor do Título:";
            // 
            // txtValorTitulo
            // 
            this.txtValorTitulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorTitulo.Location = new System.Drawing.Point(135, 219);
            this.txtValorTitulo.MaxLength = 21;
            this.txtValorTitulo.Name = "txtValorTitulo";
            this.txtValorTitulo.Size = new System.Drawing.Size(161, 20);
            this.txtValorTitulo.TabIndex = 40;
            this.txtValorTitulo.Leave += new System.EventHandler(this.txtValorTitulo_Leave);
            // 
            // lblValorProtestar
            // 
            this.lblValorProtestar.AutoSize = true;
            this.lblValorProtestar.Location = new System.Drawing.Point(43, 247);
            this.lblValorProtestar.Name = "lblValorProtestar";
            this.lblValorProtestar.Size = new System.Drawing.Size(86, 13);
            this.lblValorProtestar.TabIndex = 41;
            this.lblValorProtestar.Text = "* Valor Protestar:";
            // 
            // txtValorProtestar
            // 
            this.txtValorProtestar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorProtestar.Location = new System.Drawing.Point(135, 245);
            this.txtValorProtestar.MaxLength = 21;
            this.txtValorProtestar.Name = "txtValorProtestar";
            this.txtValorProtestar.Size = new System.Drawing.Size(161, 20);
            this.txtValorProtestar.TabIndex = 42;
            this.txtValorProtestar.Leave += new System.EventHandler(this.txtValorProtestar_Leave);
            // 
            // lblDataEmissao
            // 
            this.lblDataEmissao.AutoSize = true;
            this.lblDataEmissao.Location = new System.Drawing.Point(32, 274);
            this.lblDataEmissao.Name = "lblDataEmissao";
            this.lblDataEmissao.Size = new System.Drawing.Size(97, 13);
            this.lblDataEmissao.TabIndex = 44;
            this.lblDataEmissao.Text = "* Data de Emissão:";
            // 
            // mtbDataEmissao
            // 
            this.mtbDataEmissao.Location = new System.Drawing.Point(135, 271);
            this.mtbDataEmissao.Mask = "00/00/0000";
            this.mtbDataEmissao.Name = "mtbDataEmissao";
            this.mtbDataEmissao.Size = new System.Drawing.Size(86, 20);
            this.mtbDataEmissao.TabIndex = 43;
            this.mtbDataEmissao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mtbDataEmissao.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.mtbDataEmissao.Leave += new System.EventHandler(this.mtbDataEmissao_Leave);
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.AutoSize = true;
            this.lblDataVencimento.Location = new System.Drawing.Point(15, 300);
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.Size = new System.Drawing.Size(114, 13);
            this.lblDataVencimento.TabIndex = 46;
            this.lblDataVencimento.Text = "* Data do Vencimento:";
            // 
            // mtbDataVencimento
            // 
            this.mtbDataVencimento.Location = new System.Drawing.Point(135, 297);
            this.mtbDataVencimento.Mask = "00/00/0000";
            this.mtbDataVencimento.Name = "mtbDataVencimento";
            this.mtbDataVencimento.Size = new System.Drawing.Size(86, 20);
            this.mtbDataVencimento.TabIndex = 45;
            this.mtbDataVencimento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mtbDataVencimento.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.mtbDataVencimento.Leave += new System.EventHandler(this.mtbDataVencimento_Leave);
            // 
            // lblTipoDocumento
            // 
            this.lblTipoDocumento.AutoSize = true;
            this.lblTipoDocumento.Location = new System.Drawing.Point(18, 325);
            this.lblTipoDocumento.Name = "lblTipoDocumento";
            this.lblTipoDocumento.Size = new System.Drawing.Size(111, 13);
            this.lblTipoDocumento.TabIndex = 48;
            this.lblTipoDocumento.Text = "* Tipo de Documento:";
            // 
            // txtTipoDocumento
            // 
            this.txtTipoDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTipoDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTipoDocumento.Location = new System.Drawing.Point(135, 323);
            this.txtTipoDocumento.MaxLength = 10;
            this.txtTipoDocumento.Name = "txtTipoDocumento";
            this.txtTipoDocumento.Size = new System.Drawing.Size(86, 20);
            this.txtTipoDocumento.TabIndex = 47;
            // 
            // lblOperacao
            // 
            this.lblOperacao.AutoSize = true;
            this.lblOperacao.Location = new System.Drawing.Point(65, 351);
            this.lblOperacao.Name = "lblOperacao";
            this.lblOperacao.Size = new System.Drawing.Size(64, 13);
            this.lblOperacao.TabIndex = 50;
            this.lblOperacao.Text = "* Operação:";
            // 
            // txtOperacao
            // 
            this.txtOperacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOperacao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOperacao.Location = new System.Drawing.Point(135, 349);
            this.txtOperacao.MaxLength = 1;
            this.txtOperacao.Name = "txtOperacao";
            this.txtOperacao.Size = new System.Drawing.Size(86, 20);
            this.txtOperacao.TabIndex = 49;
            // 
            // lblValorParcela
            // 
            this.lblValorParcela.AutoSize = true;
            this.lblValorParcela.Location = new System.Drawing.Point(41, 377);
            this.lblValorParcela.Name = "lblValorParcela";
            this.lblValorParcela.Size = new System.Drawing.Size(88, 13);
            this.lblValorParcela.TabIndex = 51;
            this.lblValorParcela.Text = "Valor da Parcela:";
            // 
            // txtValorParcela
            // 
            this.txtValorParcela.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorParcela.Location = new System.Drawing.Point(135, 375);
            this.txtValorParcela.MaxLength = 21;
            this.txtValorParcela.Name = "txtValorParcela";
            this.txtValorParcela.Size = new System.Drawing.Size(161, 20);
            this.txtValorParcela.TabIndex = 52;
            this.txtValorParcela.Leave += new System.EventHandler(this.txtValorParcela_Leave);
            // 
            // lblQtdeParcela
            // 
            this.lblQtdeParcela.AutoSize = true;
            this.lblQtdeParcela.Location = new System.Drawing.Point(5, 403);
            this.lblQtdeParcela.Name = "lblQtdeParcela";
            this.lblQtdeParcela.Size = new System.Drawing.Size(124, 13);
            this.lblQtdeParcela.TabIndex = 53;
            this.lblQtdeParcela.Text = "Quantidade de Parcelas:";
            // 
            // txtQtdeParcelas
            // 
            this.txtQtdeParcelas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQtdeParcelas.Location = new System.Drawing.Point(135, 401);
            this.txtQtdeParcelas.MaxLength = 8;
            this.txtQtdeParcelas.Name = "txtQtdeParcelas";
            this.txtQtdeParcelas.Size = new System.Drawing.Size(161, 20);
            this.txtQtdeParcelas.TabIndex = 54;
            this.txtQtdeParcelas.Leave += new System.EventHandler(this.txtQtdeParcelas_Leave);
            // 
            // frmTituloCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 476);
            this.Controls.Add(this.tlpCadastro);
            this.MaximizeBox = false;
            this.Name = "frmTituloCadastro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmTituloCadastro";
            this.tlpCadastro.ResumeLayout(false);
            this.pnlCadastro.ResumeLayout(false);
            this.pnlCadastro.PerformLayout();
            this.pnlBotao.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpCadastro;
        private System.Windows.Forms.Panel pnlCadastro;
        private System.Windows.Forms.Label lblCodigoInterno;
        private System.Windows.Forms.TextBox txtCodigoInterno;
        private System.Windows.Forms.Panel pnlBotao;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Label lblBancoId;
        private System.Windows.Forms.TextBox txtTituloId;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Label lblUFPag;
        private System.Windows.Forms.TextBox txtUFPag;
        private System.Windows.Forms.Label lblCidadePag;
        private System.Windows.Forms.TextBox txtCidadePag;
        private System.Windows.Forms.ComboBox cboDevedor;
        private System.Windows.Forms.Label lblDevedor;
        private System.Windows.Forms.ComboBox cboBanco;
        private System.Windows.Forms.Label lblBanco;
        private System.Windows.Forms.Label lblParcela;
        private System.Windows.Forms.TextBox txtParcela;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.TextBox txtTitulo;
        private System.Windows.Forms.Label lblOperacao;
        private System.Windows.Forms.TextBox txtOperacao;
        private System.Windows.Forms.Label lblTipoDocumento;
        private System.Windows.Forms.TextBox txtTipoDocumento;
        private System.Windows.Forms.Label lblDataVencimento;
        private System.Windows.Forms.MaskedTextBox mtbDataVencimento;
        private System.Windows.Forms.Label lblDataEmissao;
        private System.Windows.Forms.MaskedTextBox mtbDataEmissao;
        private System.Windows.Forms.Label lblValorProtestar;
        private System.Windows.Forms.TextBox txtValorProtestar;
        private System.Windows.Forms.Label lblValorTitulo;
        private System.Windows.Forms.TextBox txtValorTitulo;
        private System.Windows.Forms.Label lblQtdeParcela;
        private System.Windows.Forms.TextBox txtQtdeParcelas;
        private System.Windows.Forms.Label lblValorParcela;
        private System.Windows.Forms.TextBox txtValorParcela;
    }
}