﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmTituloCadastro : Form
    {
        public frmTituloCadastro()
        {
            InitializeComponent();
            txtCodigoInterno.Enabled = true;
            this.Text = "Cadastrar Novo Titulo";

            PopularComboBanco();
            PopularComboDevedor();
        }

        public frmTituloCadastro(int usuarioId)
        {
            InitializeComponent();
            txtCodigoInterno.Enabled = false;
            PopularComboBanco();
            PopularComboDevedor();
            ExibirInformacoes(usuarioId);
            this.Text = "Editar Titulo Cadastrado";
        }

        #region métodos

        private async void PopularComboBanco()
        {
            cboBanco.DataSource = null;
            cboBanco.Items.Clear();

            BancoBO bancoBO = new BancoBO();
            await bancoBO.GetBancosAsync(Program.API_URL);

            cboBanco.DataSource = bancoBO.listaBancos;
            cboBanco.ValueMember = "bancoId";
            cboBanco.DisplayMember = "nome";
            cboBanco.SelectedIndex = -1;            
        }

        private async void PopularComboDevedor()
        {
            cboDevedor.DataSource = null;
            cboDevedor.Items.Clear();

            DevedorBO devedorBO = new DevedorBO();
            await devedorBO.GetDevedoresAsync(Program.API_URL);

            cboDevedor.DataSource = devedorBO.listaDevedores;
            cboDevedor.ValueMember = "devedorId";
            cboDevedor.DisplayMember = "nome";
            cboDevedor.SelectedIndex = -1;
        }

        private async void ExibirInformacoes(int usuarioId)
        {
            TituloBO tituloBO = new TituloBO();
            await tituloBO.GetTituloAsync(Program.API_URL, usuarioId);

            if (tituloBO.titulo != null)
            {
                txtTituloId.Text = tituloBO.titulo.TituloId.ToString();
                cboBanco.SelectedValue = tituloBO.titulo.BancoId.ToString();
                cboDevedor.SelectedValue = tituloBO.titulo.DevedorId.ToString();
                txtCodigoInterno.Text = tituloBO.titulo.CodigoInterno;
                txtTitulo.Text = tituloBO.titulo.NumeroTitulo.ToString();
                txtParcela.Text = tituloBO.titulo.Parcela.ToString();
                txtCidadePag.Text = tituloBO.titulo.CidadePag;
                txtUFPag.Text = tituloBO.titulo.UfPag;
                txtValorTitulo.Text = tituloBO.titulo.ValorTitulo.ToString();
                txtValorProtestar.Text = tituloBO.titulo.ValorProtestar.ToString();
                mtbDataEmissao.Text = tituloBO.titulo.DataEmissao.ToShortDateString();
                mtbDataVencimento.Text = tituloBO.titulo.DataVencimento.ToShortDateString();
                txtTipoDocumento.Text = tituloBO.titulo.TipoDocumento;
                txtOperacao.Text = tituloBO.titulo.Operacao;
                txtValorParcela.Text = tituloBO.titulo.ValorParcela.ToString();
                txtQtdeParcelas.Text = tituloBO.titulo.QtdeParcela.ToString();
            }
        }

        #endregion

        #region eventos

        private void txtCodigoInterno_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCodigoInterno.Text))
            {
                if (!long.TryParse(txtCodigoInterno.Text, out long cpf))
                {
                    MessageBox.Show("O Código Interno deve conter apenas números!");
                    txtCodigoInterno.Clear();
                    txtCodigoInterno.Focus();
                }
            }
        }

        private void txtTitulo_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtTitulo.Text))
            {
                if (!int.TryParse(txtTitulo.Text, out int codigo))
                {
                    MessageBox.Show("O Número do Título deve conter apenas números!");
                    txtTitulo.Clear();
                    txtTitulo.Focus();
                }
            }
        }

        private void txtParcela_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtParcela.Text))
            {
                if (!int.TryParse(txtParcela.Text, out int codigo))
                {
                    MessageBox.Show("O Número da Parcela deve conter apenas números!");
                    txtParcela.Clear();
                    txtParcela.Focus();
                }
            }
        }

        private void txtValorTitulo_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtValorTitulo.Text))
            {
                if (!decimal.TryParse(txtValorTitulo.Text, out decimal codigo))
                {
                    MessageBox.Show("O Valor do Título deve conter apenas números e vírgula!");
                    txtValorTitulo.Clear();
                    txtValorTitulo.Focus();
                }
            }
        }

        private void txtValorProtestar_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtValorProtestar.Text))
            {
                if (!decimal.TryParse(txtValorProtestar.Text, out decimal codigo))
                {
                    MessageBox.Show("O Valor para Protestar deve conter apenas números e vírgula!");
                    txtValorProtestar.Clear();
                    txtValorProtestar.Focus();
                }
            }
        }

        private void mtbDataEmissao_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(mtbDataEmissao.Text))
            {
                if (!mtbDataEmissao.MaskCompleted)
                {
                    MessageBox.Show("A Data de Emissão não foi preenchida corretamente!");
                    mtbDataEmissao.Clear();
                    mtbDataEmissao.Focus();
                }
                else
                {
                    mtbDataEmissao.TextMaskFormat = MaskFormat.IncludeLiterals;
                    if (!DateTime.TryParse(mtbDataEmissao.Text, out DateTime data))
                    {
                        MessageBox.Show("A Data de Emissão não foi preenchida corretamente!");
                        mtbDataEmissao.Clear();
                        mtbDataEmissao.Focus();
                    }
                    mtbDataEmissao.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals ;
                }
            }
        }

        private void mtbDataVencimento_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(mtbDataVencimento.Text))
            {
                if (!mtbDataVencimento.MaskCompleted)
                {
                    MessageBox.Show("A Data de Vencimento não foi preenchida corretamente!");
                    mtbDataVencimento.Clear();
                    mtbDataVencimento.Focus();
                }
                else
                {
                    mtbDataVencimento.TextMaskFormat = MaskFormat.IncludeLiterals;
                    if (!DateTime.TryParse(mtbDataVencimento.Text, out DateTime data))
                    {
                        MessageBox.Show("A Data de Vencimento não foi preenchida corretamente!");
                        mtbDataVencimento.Clear();
                        mtbDataVencimento.Focus();
                    }
                    mtbDataVencimento.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
                }
            }
        }

        private void txtValorParcela_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtValorParcela.Text))
            {
                if (!decimal.TryParse(txtValorParcela.Text, out decimal codigo))
                {
                    MessageBox.Show("O Valor da Parcela deve conter apenas números e vírgula!");
                    txtValorParcela.Clear();
                    txtValorParcela.Focus();
                }
            }
        }

        private void txtQtdeParcelas_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtQtdeParcelas.Text))
            {
                if (!int.TryParse(txtQtdeParcelas.Text, out int codigo))
                {
                    MessageBox.Show("A quantidade de parcelas deve conter apenas números!");
                    txtQtdeParcelas.Clear();
                    txtQtdeParcelas.Focus();
                }
            }
        }

        private async void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                mtbDataEmissao.TextMaskFormat = MaskFormat.IncludeLiterals;
                mtbDataVencimento.TextMaskFormat = MaskFormat.IncludeLiterals;

                Titulo titulo = new Titulo
                {
                    TituloId = string.IsNullOrEmpty(txtTituloId.Text) ? 0 : Convert.ToInt32(txtTituloId.Text),
                    BancoId = cboBanco.SelectedValue == null ? 0 : Convert.ToInt32(cboBanco.SelectedValue),
                    DevedorId = cboDevedor.SelectedValue == null ? 0 : Convert.ToInt32(cboDevedor.SelectedValue),
                    CodigoInterno = txtCodigoInterno.Text.Trim(),
                    NumeroTitulo = string.IsNullOrEmpty(txtTitulo.Text) ? 0 : Convert.ToInt32(txtTitulo.Text),
                    Parcela = string.IsNullOrEmpty(txtParcela.Text) ? 0 : Convert.ToInt32(txtParcela.Text),
                    CidadePag = txtCidadePag.Text.Trim(),
                    UfPag = txtUFPag.Text.Trim(),
                    ValorTitulo = string.IsNullOrEmpty(txtValorTitulo.Text) ? 0 : Convert.ToDecimal(txtValorTitulo.Text),
                    ValorProtestar = string.IsNullOrEmpty(txtValorProtestar.Text) ? 0 : Convert.ToDecimal(txtValorProtestar.Text),
                    TipoDocumento = txtTipoDocumento.Text.Trim(),
                    Operacao = txtOperacao.Text.Trim(),
                    ValorParcela = string.IsNullOrEmpty(txtValorParcela.Text) ? 0 : Convert.ToDecimal(txtValorParcela.Text),
                    QtdeParcela = string.IsNullOrEmpty(txtQtdeParcelas.Text) ? 0 : Convert.ToInt32(txtQtdeParcelas.Text),
                };

                if (!string.IsNullOrEmpty(mtbDataEmissao.Text))
                {
                    titulo.DataEmissao = Convert.ToDateTime(mtbDataEmissao.Text);
                }

                if (!string.IsNullOrEmpty(mtbDataVencimento.Text))
                {
                    titulo.DataVencimento = Convert.ToDateTime(mtbDataVencimento.Text);
                }

                TituloBO tituloBO = new TituloBO();
                await tituloBO.SalvarTitulo(Program.API_URL, titulo);

                if (tituloBO.resultado)
                {
                    MessageBox.Show("Dados salvos com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Não foi possível salvar os dados, operação cancelada!", "Operação Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                mtbDataEmissao.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
                mtbDataVencimento.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            }
        }

        private async void btnExcluir_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Confirma a exclusão?", "Confirmação?", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes)
            {
                return;
            }

            int tituloId = string.IsNullOrEmpty(txtTituloId.Text) ? 0 : Convert.ToInt32(txtTituloId.Text);

            TituloBO tituloBO = new TituloBO();
            await tituloBO.DeleteTituloAsync(Program.API_URL, tituloId);

            if (tituloBO.resultado)
            {
                MessageBox.Show("Dados excluídos com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            else
            {
                MessageBox.Show("Não foi possível excluir os dados, operação cancelada!", "Operação Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion
    }
}
