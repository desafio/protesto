﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmTituloCarga : Form
    {
        public frmTituloCarga()
        {
            InitializeComponent();
        }

        private void btnSelecionarArquivo_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog
            {
                Filter = "Arquivos de Texto | *.txt"                
            };
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtCaminhoArquivo.Text = fileDialog.FileName;
                btnImportarArquivo.Enabled = true;
            }            
        }

        private async void btnImportarArquivo_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCaminhoArquivo.Text))
            {
                if (File.Exists(txtCaminhoArquivo.Text))
                {                    
                    StreamReader streamArq = new StreamReader(txtCaminhoArquivo.Text);
                    int numeroLinha = 1;
                    string sLine = string.Empty;
                    string[] dados;
                    //List<string[]> listaDados = new List<string[]>();                    

                    while (sLine != null)
                    {
                        sLine = streamArq.ReadLine();
                        if (sLine != null)
                        {
                            dados = sLine.Split(("\t").ToCharArray());

                            if (numeroLinha > 1)
                            { 
                                // Banco
                                Banco banco = new Banco
                                {
                                    CodigoBanco = Convert.ToInt32(dados[0]),
                                    Nome = dados[2]
                                };
                                await new BancoBO().SalvarBanco(Program.API_URL, banco);

                                // Devedor
                                Devedor devedor = new Devedor
                                {
                                    Nome = dados[5]
                                    , Cpf = dados[6].Replace(".", "").Replace("-", "").Replace("/", "")
                                    , Endereco = dados[7]
                                    , Bairro = dados[8]
                                    , Cidade = dados[9]
                                    , Cep = dados[10].Replace("-","")
                                    , Uf = dados[11]                                
                                };
                                await new DevedorBO().SalvarDevedor(Program.API_URL, devedor);

                                // Título
                                Titulo titulo = new Titulo
                                {
                                    BancoId = banco.BancoId
                                    , DevedorId = devedor.DevedorId
                                    , CodigoInterno = dados[1]
                                    , NumeroTitulo = string.IsNullOrEmpty(dados[3]) ? 0 : Convert.ToInt32(dados[3])
                                    , Parcela = string.IsNullOrEmpty(dados[4]) ? 0 : Convert.ToInt32(dados[4])
                                    , CidadePag = dados[12]
                                    , UfPag = dados[13]
                                    , ValorTitulo = string.IsNullOrEmpty(dados[14]) ? 0 : Convert.ToDecimal(dados[14])
                                    , ValorProtestar = string.IsNullOrEmpty(dados[15]) ? 0 : Convert.ToDecimal(dados[15])
                                    , TipoDocumento = dados[18]
                                    , Operacao = dados[19]
                                    , ValorParcela = string.IsNullOrEmpty(dados[20]) ? 0 : Convert.ToDecimal(dados[20])
                                    , QtdeParcela = string.IsNullOrEmpty(dados[21]) ? 0 : Convert.ToInt32(dados[21])
                                };
                                if (!string.IsNullOrEmpty(dados[16]))
                                {
                                    titulo.DataEmissao = Convert.ToDateTime(dados[16]);
                                }
                                if (!string.IsNullOrEmpty(dados[17]))
                                {
                                    titulo.DataVencimento = Convert.ToDateTime(dados[17]);
                                }
                                await new TituloBO().SalvarTitulo(Program.API_URL, titulo);
                            }
                        }
                        numeroLinha++;
                    }
                    streamArq.Close();

                    //dgvResultado.DataSource = listaDados;

                    MessageBox.Show("Arquivo importado com sucesso!");
                }
                else
                {
                    MessageBox.Show("Arquivo de Texto não encontrado!");
                }
            }
        }
    }
}
