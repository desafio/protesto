﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmTituloPesquisa : Form
    {
        public frmTituloPesquisa()
        {
            InitializeComponent();
            PopularGridTitulos();
        }

        #region Métodos


        private async void PopularGridTitulos()
        {
            lblTotal.Text = "* * *";

            //popular grid
            TituloBO tituloBO = new TituloBO();
            await tituloBO.GetTitulosAsync(Program.API_URL);

            dgvDados.DataSource = tituloBO.listaTitulos;
            PersonalizarGridTitulos();
            lblTotal.Text = $"Titulos: {dgvDados.RowCount}";
        }

        private void PersonalizarGridTitulos()
        {
            //titulo                       
            /*
            dgvDados.Columns["tituloId"].HeaderText = "Titulo Id";
            dgvDados.Columns["cpf"].HeaderText = "CPF";
            dgvDados.Columns["nome"].HeaderText = "Nome";
            dgvDados.Columns["cep"].HeaderText = "CEP";
            dgvDados.Columns["endereco"].HeaderText = "Endereço";
            dgvDados.Columns["bairro"].HeaderText = "Bairro";
            dgvDados.Columns["cidade"].HeaderText = "Cidade";
            dgvDados.Columns["uf"].HeaderText = "UF";
            */

            //alinhar
            foreach (DataGridViewColumn column in dgvDados.Columns)
            {
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                if (column.DataPropertyName == "tituloId")
                {
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
            }

            // criar link
            dgvDados.Columns["tituloId"].DefaultCellStyle.Font = new Font(Font, FontStyle.Underline);
            dgvDados.Columns["tituloId"].DefaultCellStyle.ForeColor = Color.Blue;
            dgvDados.Columns["tituloId"].DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
        }

        #endregion

        private void dgvTitulos_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex == dgvDados.Columns["tituloId"].Index)
            {
                dgvDados.Cursor = Cursors.Hand;
            }
            else
            {
                dgvDados.Cursor = Cursors.Arrow;
            }
        }

        private void dgvTitulos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //EDITAR REGISTRO DA GRID
            if (e.ColumnIndex == dgvDados.Columns["tituloId"].Index && dgvDados.Cursor == Cursors.Hand)
            {
                int tituloId = Convert.ToInt32(dgvDados.Rows[e.RowIndex].Cells["tituloId"].Value.ToString());

                frmTituloCadastro cad = new frmTituloCadastro(tituloId);
                cad.ShowDialog();

                PopularGridTitulos();
            }
        }

        private void dgvDados_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex == dgvDados.Columns["tituloId"].Index)
            {
                dgvDados.Cursor = Cursors.Hand;
            }
            else
            {
                dgvDados.Cursor = Cursors.Arrow;
            }
        }

        private void dgvDados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //EDITAR REGISTRO DA GRID
            if (e.ColumnIndex == dgvDados.Columns["tituloId"].Index && dgvDados.Cursor == Cursors.Hand)
            {
                int tituloId = Convert.ToInt32(dgvDados.Rows[e.RowIndex].Cells["tituloId"].Value.ToString());

                frmTituloCadastro cad = new frmTituloCadastro(tituloId);
                cad.ShowDialog();

                PopularGridTitulos();
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmTituloCadastro cad = new frmTituloCadastro();
            cad.ShowDialog();

            PopularGridTitulos();
        }
    }
}
