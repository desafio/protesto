﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmUsuarioCadastro : Form
    {  
        public frmUsuarioCadastro()
        {
            InitializeComponent();
            txtLogin.Enabled = true;
            this.Text = "Cadastrar Novo Usuário";
            btnExcluir.Visible = false;
        }

        public frmUsuarioCadastro(int usuarioId)
        {
            InitializeComponent();
            txtLogin.Enabled = false;
            ExibirInformacoes(usuarioId);
            this.Text = "Editar Usuário Cadastrado";
            btnExcluir.Visible = true;
        }

        #region métodos

        private bool ValidarSalvar()
        {
            //LOGIN
            if (txtLogin.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Obrigatório preencher o Login!", "Falta Informação Obrigatória", MessageBoxButtons.OK, MessageBoxIcon.Warning);                
                txtLogin.Focus();
                return false;
            }

            //SENHAS
            if (txtSenha.Text.Trim() != string.Empty && txtConfirmarSenha.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Para alterar a Senha é obrigatório preencher a \"Senha\" e \"Confirmar Senha\"!", "Falta Confirmar a Senha", MessageBoxButtons.OK, MessageBoxIcon.Warning);                
                txtConfirmarSenha.Focus();
                return false;
            }

            if (txtSenha.Text.Trim() == string.Empty && txtConfirmarSenha.Text.Trim() != string.Empty)
            {
                MessageBox.Show("Para alterar a Senha é obrigatório preencher a \"Senha\" e a \"Confirmar Senha\"!", "Falta a Senha", MessageBoxButtons.OK, MessageBoxIcon.Warning);                
                txtSenha.Focus();
                return false;
            }

            if (txtSenha.Text.Trim() != txtConfirmarSenha.Text.Trim())
            {
                MessageBox.Show("\"Senha\" e \"Confirmar Senha\" são divergentes!", "Senhas não são iguais", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtSenha.Focus();
                return false;
            }

            //E-MAIL
            if (txtEmail.Text.Trim() != string.Empty)
            {
                if (!UtilBO.IsEmail(txtEmail.Text))
                {
                    MessageBox.Show("E-mail informado não é um email válido!", "E-mail Inválido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtEmail.Focus();
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region Métodos

        private async void ExibirInformacoes(int usuarioId)
        {
            UsuarioBO usuarioBO = new UsuarioBO();
            await usuarioBO.GetUsuarioAsync(Program.API_URL, usuarioId);

            if (usuarioBO.usuario != null)
            {
                txtUsuarioId.Text = usuarioBO.usuario.UsuarioId.ToString();
                txtLogin.Text = usuarioBO.usuario.Login;
                txtSenha.Text = string.Empty;
                txtConfirmarSenha.Text = string.Empty;
                txtEmail.Text = usuarioBO.usuario.Email;
            }
        }

        #endregion

        #region eventos

        private async void btnSalvar_Click(object sender, EventArgs e)
        {
            if (ValidarSalvar())
            {
                Usuario usuario = new Usuario
                {
                    UsuarioId = string.IsNullOrEmpty(txtUsuarioId.Text) ? 0 : Convert.ToInt32(txtUsuarioId.Text),
                    Login = txtLogin.Text.Trim(),
                    Senha = txtSenha.Text.Trim(),
                    Email = txtEmail.Text.Trim()
                };

                UsuarioBO usuarioBO = new UsuarioBO();
                await usuarioBO.SalvarUsuario(Program.API_URL, usuario);

                if (usuarioBO.resultado)
                {
                    MessageBox.Show("Dados salvos com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Não foi possível salvar os dados, operação cancelada!", "Operação Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }        

        private async void btnExcluir_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Confirma a exclusão?", "Confirmação?", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes)
            {
                return;
            }

            int UsuarioId = string.IsNullOrEmpty(txtUsuarioId.Text) ? 0 : Convert.ToInt32(txtUsuarioId.Text);

            UsuarioBO usuarioBO = new UsuarioBO();
            await usuarioBO.DeleteUsuarioAsync(Program.API_URL, UsuarioId);

            if (usuarioBO.resultado)
            {
                MessageBox.Show("Dados excluídos com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            else
            {
                MessageBox.Show("Não foi possível excluir os dados, operação cancelada!", "Operação Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion
    }
}
