﻿using Protesto.WIN.BLL.BO;
using Protesto.WIN.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Protesto.WIN.Visual
{
    public partial class frmUsuarioPesquisa : Form
    {
        public frmUsuarioPesquisa()
        {
            InitializeComponent();
            PopularGridUsuarios();
        }

        #region Métodos


        private async void PopularGridUsuarios()
        {
            lblTotal.Text = "* * *";

            //popular grid
            UsuarioBO usuarioBO = new UsuarioBO();            
            await usuarioBO.GetUsuariosAsync(Program.API_URL);

            dgvUsuarios.DataSource = usuarioBO.listaUsuarios;
            PersonalizarGridUsuarios();
            lblTotal.Text = $"Usuários: {dgvUsuarios.RowCount}";
        }

        private void PersonalizarGridUsuarios()
        {
            //titulo                       
            dgvUsuarios.Columns["usuarioId"].HeaderText = "Usuário Id";
            dgvUsuarios.Columns["login"].HeaderText = "Login";
            dgvUsuarios.Columns["senha"].HeaderText = "Senha";
            dgvUsuarios.Columns["email"].HeaderText = "E-mail";            

            //ocultar
            dgvUsuarios.Columns["senha"].Visible = false;

            //alinhar
            foreach (DataGridViewColumn column in dgvUsuarios.Columns)
            {
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                if (column.DataPropertyName == "usuarioId")
                {
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
            }

            // criar link
            dgvUsuarios.Columns["usuarioId"].DefaultCellStyle.Font = new Font(Font, FontStyle.Underline);
            dgvUsuarios.Columns["usuarioId"].DefaultCellStyle.ForeColor = Color.Blue;
            dgvUsuarios.Columns["usuarioId"].DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
        }

        #endregion

        private void dgvUsuarios_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex == dgvUsuarios.Columns["usuarioId"].Index)
            {
                dgvUsuarios.Cursor = Cursors.Hand;
            }
            else
            {
                dgvUsuarios.Cursor = Cursors.Arrow;
            }
        }

        private void dgvUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //EDITAR REGISTRO DA GRID
            if (e.ColumnIndex == dgvUsuarios.Columns["usuarioId"].Index && dgvUsuarios.Cursor == Cursors.Hand)
            {
                int usuarioId = Convert.ToInt32(dgvUsuarios.Rows[e.RowIndex].Cells["usuarioId"].Value.ToString());

                frmUsuarioCadastro cad = new frmUsuarioCadastro(usuarioId);
                cad.ShowDialog();

                PopularGridUsuarios();
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            frmUsuarioCadastro cad = new frmUsuarioCadastro();
            cad.ShowDialog();

            PopularGridUsuarios();
        }
    }
}
